#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <ctype.h>
#include <time.h>
#include <unistd.h>
#include <math.h>
#include <alsa/asoundlib.h>

#define SAMPLE_RATE 44100
#define BEEP_DURATION 0.5
#define BEEP_FREQUENCY 500

int play_beep(void) {
    snd_pcm_t* handle;
    snd_pcm_sframes_t frames;
    int rc = snd_pcm_open(&handle, "default", SND_PCM_STREAM_PLAYBACK, 0);

    if (rc < 0) {
        fprintf(stderr, "unable to open PCM device: %s\n", snd_strerror(rc));
        return -1;
    }

    snd_pcm_set_params(handle,
                        SND_PCM_FORMAT_FLOAT,
                        SND_PCM_ACCESS_RW_INTERLEAVED,
                        1,
                        SAMPLE_RATE,
                        1,
                        500000);
    
    int num_frames = SAMPLE_RATE * BEEP_DURATION;
    float* buffer = malloc(num_frames * sizeof(float));

    for (int i = 0; i < num_frames; i++) {
        buffer[i] = sin(2 * M_PI * BEEP_FREQUENCY * i / SAMPLE_RATE);
    }

    frames = snd_pcm_writei(handle, buffer, num_frames);
    if (frames < 0) frames = snd_pcm_recover(handle, frames, 0);
    if (frames < 0) {
        fprintf(stderr, "snd_pcm_writei failed: %s\n", snd_strerror(frames));
        return -1;
    }

    snd_pcm_drain(handle);
    snd_pcm_close(handle);
    free(buffer);
    return 0;
}

/* function to check for valid time format */
bool check_valid_time_format(const char* str) {
    /* HH:MM:SS */
    for (int i = 0; i < 8; i++) {
        if ((i == 2 || i == 5)) {
            if (str[i] != ':') { return false; }
        } else {
            if (!isdigit((unsigned char)str[i])) { return false; }
        }
    }
    return true;
}

int extract_hours(const char* str) {
    char buf[3] = {0};
    strncpy(buf, str, 2);
    buf[2] = '\0';
    return atoi(buf);
}

int extract_minutes(const char* str) {
    char buf[3] = {0};
    strncpy(buf, str + 3, 2);
    buf[2] = '\0';
    return atoi(buf);
}

int extract_seconds(const char* str) {
    char buf[3] = {0};
    strncpy(buf, str + 6, 2);
    buf[2] = '\0';
    return atoi(buf);
}

int parse_time(char* time) {
    if (strlen(time) != 8) {
        printf("%s\n", "given time is not 8 characters long");
        return -1;
    }

    if (!check_valid_time_format(time)) {
        printf("%s\n", "invalid time format");
        return -1;
    }

    int hours = extract_hours(time);
    int mins = extract_minutes(time);
    int seconds = extract_seconds(time);

    int convert_to_seconds = hours * 3600 + mins * 60 + seconds;

    return convert_to_seconds;
}

void start_timer(int seconds) {
    time_t raw_time = time(NULL);
    struct tm time_info;
    struct tm* result = gmtime_r(&raw_time, &time_info);
    if (result == NULL) {
        printf("%s\n", "failed to get time");
        exit(1);
    }
    char time_buf[9];
    
    while (seconds > 0) {
        time_info.tm_hour = seconds / 3600;
        time_info.tm_min = (seconds % 3600) / 60;
        time_info.tm_sec = seconds % 60;

        strftime(time_buf, sizeof(time_buf), "%H:%M:%S", &time_info);

        printf("\rtime left: %s", time_buf);
        fflush(stdout);

        sleep(1);
        seconds--;
    }
    printf("\n%s\n", "time's up!");

    struct timespec ts;
    ts.tv_sec = 0;
    ts.tv_nsec = 100 * 5000000;

    for (int i = 0; i < 3; i++) {
        int beep_ret = play_beep();
        if (beep_ret == -1) {
            printf("%s\n", "failed to play a beep sound");
            exit(1);
        }
        nanosleep(&ts, NULL);
    }
}

void usage(void) {
    printf("%s\n%s\n", "Usage: timer [time]",
            "time format is HH:MM:SS");
}

int main(int argc, char** argv) {
    if (argc > 2) {
        usage();
        printf("%s\n", "too many arguments");

        return EXIT_FAILURE;
    } else if (argc == 2) {
        char* time = argv[1];
        int time_in_seconds = parse_time(time);

        if (time_in_seconds == -1) {
            printf("%s\n", "failed to parse time");
            return EXIT_FAILURE;
        } else {
            start_timer(time_in_seconds);
        }
    } else {
        usage();
        printf("%s\n", "not enough arguments");
    }

    return EXIT_SUCCESS;
}
